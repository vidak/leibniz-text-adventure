(defmacro game-action (command subj obj place &body body)
  `(progn (defun ,command (subject object)
            (if (and (eq *location* ',place)
                     (eq subject ',subj)
                     (eq object ',obj)
                     (have ',subj))
                ,@body
            '(I cannot ,command like that.)))
          (pushnew ',command *allowed-commands*)))
          
(defmacro game-action-mere-subject (command subj place &body body)
  `(progn (defun ,command (subject)
            (if (and (eq *location* ',place)
                     (eq subject ',subj)
                     (have ',subj))
                ,@body
            '(It is not possible to ,command ,subj that.)))
          (pushnew ',command *allowed-commands*)))

#|

========================
= OBJECTS INTERACTIONS =
========================

|#

;;; Beautiful Garden

(game-action-mere-subject talk leibniz beautiful-garden
			  '(Leibniz awards you his attention.
			    DID YOU NOT HEAR ME? he exclaims.
			    DO YOU DESIRETH TO LEARN OF THE MONADS? PERHAPS THE EXCELLENCE OF THE PERFECTION OF NATURE?))

(game-action talk leibniz spectacles beautiful-garden
  '(You have no idea what these spectacles are for.
    Or who they belong to.
    Are they Leibniz's spectacles?
    You ask Leibniz about the spectacles.
    OH THOSE? WHY DO YOU NOT PUT THEM ON AND BEHOLD ME IN ALL MY BEAUTY?))
    
(game-action behold leibniz spectacles beautiful-garden
  '(Leibniz allows you to witness him in all his wigg-ed glory.
    He scowls.
    He simpers.
    Truly he is an ugly smelly man.))

;;; Manor Lobby

(game-action behold leibniz spectacles manor-lobby
	     '(Leibniz is muttering to himself.
	       The topic is of course the monads.))

(game-action tickle leibniz feather-duster manor-lobby
	     '(Leibniz descends into raucous laughter.
	       You feel quite embarassed at the loud echoing of his bellowing shouts.))

;;; Street

(game-action behold leibniz spectacles street
	     '(There is a double rainbow.
	       Leibniz appears to be quite beautiful underneath it.
	       His disgustingly oily wig is reflecting its shimmering colours.))

(defparameter *cutlery-shined* nil)

(game action shine cutlery leibniz street
      (cond ((not *cutlery-cleaned*) '(You hold the silver utensil up to the sunlight.
				       There is very little effect.
				       Leibniz looks at you quizzically.
				       He wonders aloud what on earth you are doing, holding a spoon out at full arms-length in the middle of the street.))
                   (t (progn (setf *cutlery-shined* 't)
			     '(You absolutely blind Leibniz with the incredible luster of your silver spoon.
			       ...)))))

;;; Library

(game-action behold leibniz spectacles library
	     '(The smell of the wooden bookcases seems to change the way you view this smelly, dirty philosopher.))

(game-action put leibniz lecturn library
    '(Leibniz sits atop the lecturn pouting about the monads.))

;;; Dining Room

(defparameter *cutlery-cleaned* nil)

(game-action behold leibniz spectacles dining-room
	     '())

(game-action clean cutlery dish-rag dining-room
             (if (and (have 'cutlery) (not *cutlery-cleaned*))
                 (progn (setf *cutlery-cleaned* 't)
                        '(it took some work but you clean the silver eating utensil.
			  Leibniz definitely did not help.
			  He saw you bent over the table rubbing and scrubbing.
			  He merely watched you with half-interest.
			  What a terrible friend.))
               '(you do not have any cutlery.)))

;;; Drawing Room

(game-action behold leibniz spectacles drawing-room
  '(Leibniz allows you to witness him in all his wigg-ed glory.
    The smell of rich mahogany in this drawing room is intoxicating.
    These spectacles seem to be causing you to better detect odours.
    Very odd indeed.
    Oh yes, and Leibniz still smells truly, truly awful.)

;;; Kitchen

  (defparameter *wig-cooked* nil)

  (game-action behold leibniz spectacles kitchen
	       '(It is too difficult to see directly see Leibniz through the pots and pans.
		 You can, however, see his distorted reflection in a metal bowl hanging from the ceiling rack.
		 His wig looks even bigger and smellier than you ever could have imagined.))

  (game-action-mere-subject talk small-man kitchen
			    (cond ((*wig-cooked*) '(I thank you for this delicious broth.
						    May my existence in your inventory shine the happiness of the sciences of the monads upon you.))
				  ((t) '(The small man speaks to you.
					 He says he has worked at this kitchen for many years.

					 I am the head chef. He says. I have always cooked vegan food.
					 And I have never smelled anything as fascinating as the wig of that man.
					 Please let me cook his wig.))))

  (game-action cook leibniz wig kitchen
             (if (and (have 'leibniz) (not *wig-cooked*))
                 (progn (setf *wig-cooked* 't)
                        '(You wrestle Leibniz for the wig on his head.
			  He does not give it to you without a fight.
			  Leibniz is now bald.
			  You cook the wig with the aid of the small man-chef.
			  It makes a beautiful wig-flavoured stock.
			  Luckily, the wig is undamaged by the cooking process.
			  You give the wig back to Leibniz.
			  The wig is now clean. Leibniz looks sparkly and new.
			  ))
               '(You do not appear to have a Leibniz in your inventory.)))

;;; Shop Keeper's Domain

  (game-action behold leibniz spectacles shopkeepers-domain
	       '(The dappled light of the sun after the sunshower filters in through the shop window.
		 Leibniz is behind the shop counter rifling through the cash register.
		 He opens his mouth orifice and proclaims this loudly.
		 NO. THERE DO NOT APPEAR TO BE ANY MONADS IN HERE!!))

  (game-action-mere-subject talk small-man shopkeepers-domain
			    (if *wig-cooked*
				'(The small broth-cooking man speaks. My brother lives just beyond here. He is the LEXAPRO man. He will adorn you with great amounts of serotonin.)
				'(The small man-chef speaks. If only I had something delicious to serve the guests of the manor...))

;;; Warehouse

(game-action behold leibniz spectacles warehouse
  '())

;;; Secret Control Room

(defparameter *LSD-fed* nil)

(game-action behold leibniz spectacles secret-control-room
	     '(Leibniz appears to have found half a wheel of cheese laying around.
	       He fights the rodents for their meal, and easily wins.
	       He fills his mouth orifice with the mouldy yellow substance.
	       DELISHIOSH. He exclaims. THETHE RATTHS TRULY UNDERTHAND THE MONADICK POWER OF CHEETH!!))

(game-action feed leibniz LSD secret-control-room
	     (progn (setf *LSD-fed* 't)
		    '(You feed Leibniz the LSD.
	       He accepts the tab quite happily.
		      You sit around and wait an hour or so---but there appears to be no effect on the wigged philosopher.)))

(game-action-mere-subject talk leibniz
			  (if *LSD-fed*
			      '(You ask Leibniz if he feels any different.
				He speaks.
				OH THAT LITTLE MORSEL?
				THE CHEESE MADE ME HIGH TO BEGIN WITH!
				BELIEVE ME--HOWEVER--DOING THIS WAS CERTAINLY NECESSARY TO UNDERSTAND THE MONADS!
				The smelly wigg-ed man appears quite chipper.)
			      '(Leibniz speaks to you.
				OH? DO YOU NOT HAVE A LITTLE TASTY MORSEL FOR ME??
				I WOULD INDEED LIKE A LITTLE DELICIOUS TIDBIT TO NIP ME UP A LITTLE!)))

(game-action-mere-subject talk small-man
			  '())

;;; Trap Door Room

(game-action behold leibniz spectacles trap-door-room
  '())

;; Riddle of the Monads

(game-action behold leibniz spectacles riddle-of-monads
  '())

;; Science of the Monads

(game-action behold leibniz spectacles science-of-monads
  '())

(game-action weld chain bucket attic
             (if (and (have 'bucket) (not *chain-welded*))
                 (progn (setf *chain-welded* 't)
                        '(the chain is now securely welded to the bucket.))
               '(you do not have a bucket.)))

(game-action dunk bucket well garden
             (if *chain-welded* 
                 (progn (setf *bucket-filled* 't)
                        '(the bucket is now full of water))
               '(the water level is too low to reach.)))

(game-action splash bucket wizard living-room
             (cond ((not *bucket-filled*) '(the bucket has nothing in it.))
                   ((have 'frog) '(the wizard awakens and sees that you stole his frog. 
                                   he is so upset he banishes you to the 
                                   netherworlds- you lose! the end.))
                   (t '(the wizard awakens from his slumber and greets you warmly. 
                        he hands you the magic low-carb donut- you win! the end.))))


