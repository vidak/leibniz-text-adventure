(defparameter *nodes* '(
                        (beautiful-garden (You are in a beautiful garden.
					   It is replete with wonderful native trees bushes`, and shrubs.
					   The sunlight is warm and golden. Standing besides you is an angry man with a large wig.
					   He wishes to discuss the monads.))

			(manor-lobby (You are in the ostentatious lobby of a manor.
				      It is quite well-kept, really.
				      It smells like eucalyptus oil and pipe tobacco.))

			(street (You are standing in a quiet street.
				 It is quite muddy.
				 There was a sun shower just before.
				 The clouds have now parted and the weather is now quite fine!
				 ))

                        (library (This room is an enormous library.
				  There are books here on every subject known to humanity.
				  Geometry, philosophy, rhetoric---there are so many books.
				  The smell of the books is quite nice, too.))
                                                
                        (dining-room (You have entered the dining room of the manor.
				      The table is set with wonderful vegan food.
				      It looks very appetising.
				      There are a great many guests at the dinner table.
				      They do no recognise either you or Leibniz.
				      They are laughing and very much enjoying the company of each other.))
			
                        (drawing-room (You are now inside the drawing room of the manor.
				       It has many decidedly non-vegan pieces of furniture.
				       You wonder if the chef and the interior decorator should have ever had a short chat.))
			
                        (kitchen (This is the kitchen of the manor.
				  There are many utensils hanging from racks attached to the ceiling.
				  It appears as if the kitchen is ready cooking the next course))
                        
                        (shopkeepers-domain (DESCRIPTION))
                        (warehouse (DESCRIPTION))
                        (secret-control-room
			 (This is a secret boiler room, deep in the bowels of the shop warehouse. It is a secret control room for the heating of the manor. There is a control console in the middle of the room. A small man sits at the console and he turns around to face you.
			  He speaks to you. Hello there. I rather think you're going to have to give your---erm---colleague what you find in this room.
					      ))
			
                        (trap-door-room (DESCRIPTION))
                        (riddle-of-monads (DESCRIPTION))
                        (science-of-monads (DESCRIPTION))))

(defun describe-location (location nodes)
   (cadr (assoc location nodes)))

(defparameter *edges* '((beautiful-garden   (manor-lobby east door)  
                                            (street south alleyway)
                                            (library north-west gated-hedge))
                        
                        (manor-lobby        (beautiful-garden west door)
                                            (dining-room north-east double-door)
                                            (kitchen down dumb-waiter))
                        (dining-room        (manor-lobby south-west double-door)
                                            (drawing-room due-south double-door))
                        (drawing-room       (dining-room due-north double-door)
                                            (kitchen across shoot))
                        (kitchen            (manor-lobby up dumb-waiter)
                                            (drawing-room up-slightly shoot))
                                            
                        (street             (beautiful-garden north alleyway)
                                            (shopkeepers-domain forward window))
                        (shopkeepers-domain (street backward window)
                                            (warehouse behind counter))
                        (warehouse              (shopkeepers-domain front-of counter)
                                                (secret-control-room around aisle))
                        (secret-control-room    (warehouse under aisle))
                        
                        (library            (beautiful-garden south-east gated-hedge)
                                            (trap-door-room down trap-door))
                        (trap-door-room     (library up trap-door)
                                            (riddle-of-monads forward archway))
                        (riddle-of-monads   (trap-door-room backward archway)
                                            (science-of-monads forward staircase))
                        (science-of-monads  (riddle-of-monads backwards staircase))
                                            ))

(defun describe-path (edge)
  `(there is a ,(caddr edge) going ,(cadr edge) from here.))

(defun describe-paths (location edges)
  (apply #'append (mapcar #'describe-path (cdr (assoc location edges)))))

(defparameter *objects* '(leibniz spectacles lecturn wig small-man))

(defparameter *object-locations* '(


;;; Beautiful Garden

				   (leibniz beautiful-garden)
				   (spectacles beautiful-garden)

;;; Manor Lobby
				   
				   (feather-duster manor-lobby)

;; Dining Room

				   (cutlery dining-room)

;;; Drawing Room
				   
				   (object drawing-room)

;;; Kitchen
				   
				   (small-man kitchen)
				   (dish-rag kitchen)

;;; Street
				   
				   (object street)

;;; Shop Keeper's Domain
				   
				   (object shopkeepers-domain)

;;; Warehouse
				   
				   (object warehouse)

;;; Secret Control Room
				   
				   (LSD secret-control-room)

;;; Library
				   
				   (lecturn library)

;;; Trap Door Room
				   
				   (object trap-door-room)

;;; Riddle of the Monads
				   
				   (object riddle-of-monads)

;;; Science of Monads
				   
				   (object science-of-monads)

				   ))


(defun objects-at (loc objs obj-loc)
   (labels ((is-at (obj)
              (eq (cadr (assoc obj obj-loc)) loc)))
       (remove-if-not #'is-at objs)))

(defun describe-objects (loc objs obj-loc)
   (labels ((describe-obj (obj)
                `(you see a ,obj on the floor.)))
      (apply #'append (mapcar #'describe-obj (objects-at loc objs obj-loc)))))

(defparameter *location* 'beautiful-garden)

(defun look ()
  (append (describe-location *location* *nodes*)
          (describe-paths *location* *edges*)
          (describe-objects *location* *objects* *object-locations*)))

(defun walk (direction)
  (labels ((correct-way (edge)
             (eq (cadr edge) direction)))
    (let ((next (find-if #'correct-way (cdr (assoc *location* *edges*)))))
      (if next 
          (progn (setf *location* (car next)) 
                 (look))
          '(you cannot go that way.)))))

(defun pickup (object)
  (cond ((member object (objects-at *location* *objects* *object-locations*))
         (push (list object 'body) *object-locations*)
         `(you are now carrying the ,object))
	  (t `(you cannot get the ,object))))

(defun inventory ()
  (cons 'items- (objects-at 'body *objects* *object-locations*)))

(defun have (object) 
    (member object (cdr (inventory))))

(defun game-repl ()
    (let ((cmd (game-read)))
        (unless (eq (car cmd) 'quit)
            (game-print (game-eval cmd))
            (game-repl))))

(defun game-read ()
    (let ((cmd (read-from-string (concatenate 'string "(" (read-line) ")"))))
         (flet ((quote-it (x)
                    (list 'quote x)))
             (cons (car cmd) (mapcar #'quote-it (cdr cmd))))))

(defparameter *allowed-commands* '(look walk pickup inventory))

(defun game-eval (sexp)
    (if (member (car sexp) *allowed-commands*)
        (eval sexp)
        '(i do not know that command.)))

(defun tweak-text (lst caps lit)
  (when lst
    (let ((item (car lst))
          (rest (cdr lst)))
      (cond ((eql item #\space) (cons item (tweak-text rest caps lit)))
            ((member item '(#\! #\? #\.)) (cons item (tweak-text rest t lit)))
            ((eql item #\") (tweak-text rest caps (not lit)))
            (lit (cons item (tweak-text rest nil lit)))
            (caps (cons (char-upcase item) (tweak-text rest nil lit)))
            (t (cons (char-downcase item) (tweak-text rest nil nil)))))))

(defun game-print (lst)
    (princ (coerce (tweak-text (coerce (string-trim "() " (prin1-to-string lst)) 'list) t nil) 'string))
    (fresh-line))
